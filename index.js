const express = require("express");

// app - server
const app = express();

const port = 3000;

// middlewares - software that provide common services and capabilities to application outside of what's offered by OS


app.use(express.json());
app.use(express.urlencoded({extended: true}));


// [SECTION] ROUTES
// GET Method
app.get("/greet", (request, response) => {
	response.send("Hello from the /greet endpoint!");
});

app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

let users = [];

app.post("/signup", (request, response) =>{
	if(request.body.username !== "" && request.body.password !== "" )
	{
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
	}

	else
	{
		response.send("Please input BOTH username and password.");
	}
});

app.patch("/change-password", (request, response) =>{
	let message;

	for (let i=0 ; i<users.length; i++)
	{
		if(request.body.username == users[i].username)
		{
			users[i].password = request.body.password;
			message = `User ${request.body.username}'s password has been updated!`;
			break;
		}
		else
		{
			message = "User does not exist.";
		}
	}

	response.send(message);
})

// [ACTIVITY START] //

// #1 GET /home with simple message
app.get("/home", (request, response) =>{
	response.send("Welcome to the home page");
});

// #3 GET /users that retrieves all users
app.get("/users", (request, response) =>{
	response.send(users);
})

// #5 DELETE /delete-item to remove one user
app.delete("/delete-item", (request, response) => {
	response.send(`User ${request.body.username} has been deleted.`);
})

app.listen(port, () => console.log(`Server running at ${port}`));